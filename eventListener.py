import json
from nameko.events import event_handler

class EventSubscriber:
    name = 'event_listener'

    @event_handler('rest_server_event', 'say_hello')
    def handle_event(self, payload):
        data = json.loads(payload)
        print("Worker got: {}".format(data))
from flask import Flask, request
import json
import time

from nameko.standalone.rpc import ClusterRpcProxy
from nameko.standalone.events import event_dispatcher
"""
from apscheduler.schedulers.background import BackgroundScheduler

def sched_check_rpc_return():
    print("Checking rpc return")
sched = BackgroundScheduler(daemon=False)
sched.add_job(sched_check_rpc_return,'interval',seconds=2)
"""

app = Flask(__name__)

CONFIG = {'AMQP_URI': 'amqp://guest:guest@0.0.0.0:5672'}

rpc_return = {'code': '200', 'result':'Ok'}

@app.route('/hello', methods=['GET'])
def get_hello():
    with ClusterRpcProxy(CONFIG) as rpc:
        rpc_return = json.loads(rpc.test_rpc.test_method("Hello from Flask"))
        if rpc_return['code'] == '200':
            return rpc_return
        else:
            rpc_return['return'] = "Got error from worker"
            return rpc_return

@app.route('/hello_async', methods=['GET'])
def get_hello_async():
    res = None
    with ClusterRpcProxy(CONFIG) as rpc:
        hello_res = rpc.test_rpc.test_async_method.call_async("Hello from Flask")
        """ This is ultimately a blocking call"""
        res = hello_res.result()
        print(res)
        return res

"""Standalone event dispatcher works in Nameko
   as long as a proper event handler is setup.(eventListener.py)
   But receiving events needs similar queue setup using
   Kombu. As the nameko run time is not involved when running
   from Flask
   """
@app.route('/send_event', methods=['GET'])
def send_event():
    dispatch = event_dispatcher(CONFIG)
    event_data = {'data':'1000'}
    dispatch("rest_server_event", "say_hello", json.dumps(event_data))
    return "Sent event"

if __name__=='__main__':
    app.run(host='0.0.0.0', port='8000', debug=True)
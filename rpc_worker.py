from nameko.rpc import rpc
from eventListener import EventSubscriber
import json

import time

rpc_reply = {'code':'400', 'result':'Hello from RPC'}
class firstService:
    name = "test_rpc"
    result = ""
    worker = EventSubscriber()

    @rpc
    def test_method(self, test):
        return json.dumps(rpc_reply)

    @rpc
    def test_async_method(self, test):
        event_data = {'data': '1000'}
        time.sleep(3)
        """ The following will not work
        The event handler needs to create a queue.
        Which can be done using Kombo or Pika"""
        """
        CONFIG = {'AMQP_URI': 'amqp://guest:guest@0.0.0.0:5672'}
        dispatch = event_dispatcher(CONFIG)
        dispatch("micro_api_async", "say_hello", json.dumps(event_data))
        """
        return json.dumps(rpc_reply)

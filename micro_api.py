"""Code not used."""
import json
from nameko.web.handlers import http
from nameko.events import EventDispatcher
import sys
from amqp.exceptions import ConnectionError

class API:
    name = 'micro_api'
    dispatch = EventDispatcher(use_confirms=False)

    @http('GET', '/hello')
    def get_method(self, extra):
        json_data = {'name':'Arindam', 'id':'100'}
        try:
            self.dispatch("say_hello", json.dumps(json_data))
        except ConnectionRefusedError as e:
            print("Arindam: Connection refused", sys.exc_info()[0])
            pass
        return json.dumps(json_data)
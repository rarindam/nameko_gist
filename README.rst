The aim of the project is to find out how to use Nameko for our requirements.
The requirements are simplified to the following points

1. Have a Flask based rest api server, which will be catering to all the
requests coming from frontend

2. The rest server need to internally call different APIS to get the job done
requested by the front end.

3. There are two kinds of jobs:
   a. Short running jobs. Example: add two numbers, query a certain DB
   b. Long running jobs: A task which takes considerable time to finish.

4. From the point of view of microservices, both of these types can be
implemented as RPC.

5. Short running jobs are easily handled by synchronous RPC calls.
Using Nameko, these can be achieved easily, using RabbitMQ for message
broker. Example code is in Test_Flask_RPC.py, get_hello() method.
These are extremely efficient and easiy to implement, with error
propagation from remote service to calling function, handled by default.

6. Long running jobs are also handled in a similar way in Nameko.
Instead one has to use "call_async" for making an RPC call.
As shown in "get_hello_async()" method in Test_Flas_RPC.py.
Then to get the result, the code needs to result() method for
the object returned from "call_async" call.

7. Now the app can do other things before calling the result() method.
It has to randomly wait before calling result(). But this is a blocking call.
Hence the whole idea of doing something else, while the job is being
processed, is lost.

8. The other way is to disptach an event from rpc_worker.py,
which implements the RPC call. Then when the server receives the event,
it can call result() to obtain the output, in the most simplistic way.

9. Catch is this: in case where we are using Nameko, one has to implement
the REST server, using Nameko's web library, instead of Flask.
Otherwise there is no way to catch any events from a Flask app.
But using Nameko for implementing the REST server itself, limits
the flexibility of using Flask's vast array for plugins for REST server
implementation.

10. Another way to implement the same, is using Flask + Kombu, to listen
for events on specific channel. Which brings back to the question
as to why to use Nameko at all.
https://github.com/nameko/nameko/issues/459#issuecomment-318394148

11. Where Nameko shines is where the requirement is for executing
remote tasks which are short.

12. In case of long running tasks, its better to use Celery, instead
of Nameko.
This has been actually suggested by Nameko's own contributors.
Below are some of the links for reference:

https://github.com/nameko/nameko/issues/459
https://discourse.nameko.io/t/how-can-i-connect-event-callback-to-tornado/329/3
